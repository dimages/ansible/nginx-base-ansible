REGISTRY=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

### NG config
ng-config: ng-config-build ng-config-tag
ng-config-push: ng-config-build ng-config-tag ng-config-push

ng-config-build:
	docker build -t ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/config:${VERSION} -f ng-config.df .

ng-config-tag:
	docker tag ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/config:${VERSION} ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/config:latest

ng-config-push:
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/config:${VERSION} && \
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/config:latest

### NPCG config
npcg-config: npcg-config-build npcg-config-tag
npcg-config-push: npcg-config-build npcg-config-tag npcg-config-push

npcg-config-build:
	docker build -t ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/config:${VERSION} -f npcg-config.df .

npcg-config-tag:
	docker tag ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/config:${VERSION} ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/config:latest

npcg-config-push:
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/config:${VERSION} && \
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/config:latest